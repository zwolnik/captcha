#!/usr/bin/python3
from __future__ import absolute_import, division, print_function

import tensorflow as tf
from tensorflow.keras import callbacks, layers, utils

import matplotlib.pyplot as plt
import numpy as np
import cv2

import pathlib
import os
import re
import random
from datetime import datetime
from string import ascii_lowercase
AUTOTUNE = tf.data.experimental.AUTOTUNE


def get_image_paths(root):
    paths = [os.path.join(root, path) for path in os.listdir(root)]
    return paths


def get_shuffled_image_paths(root):
    paths = get_image_paths(root)
    random.shuffle(paths)
    return paths


def load_image(path):
    image = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    image = image/255.0
    assert(image is not None)
    return image


def preprocess_image(image):
    image = image[10:, 30:130, :]
    return image


def add_time(image, timestamps=5):
    shape = image.shape
    np.expand_dims(image, axis=0)
    np.broadcast_to(image, (timestamps, *shape))


def load_labels(image_paths):
    labels = [re.split("[/.]", path)[-2] for path in image_paths]
    labels = [[ch for ch in label] for label in labels]
    return labels


def outputs():
    out_nums = dict((k, str(v)) for k, v in enumerate(range(10)))
    out_chars = dict((k, v) for k, v in enumerate(ascii_lowercase, 10))
    return {**out_nums, **out_chars}


def reversed_outputs():
    outs = outputs()
    return dict(zip(outs.values(), outs.keys()))


def images_dataset(image_paths):
    image_ds = np.zeros((len(image_paths), 5, 50, 200, 4))
    for i, img in enumerate(image_paths):
        image = load_image(img)
        # image = preprocess_image(image)
        add_time(image, timestamps=5)
        image_ds[i, :, :, :, :] = image
    return image_ds


def translate_lbl(label, translator):
    translated = [translator[x] for x in label]
    return translated


def translate_labels(labels):
    r_outs = reversed_outputs()
    labels = [translate_lbl(lbl, r_outs) for lbl in labels]
    return labels


def labels_dataset(image_paths):
    lbl_ds = np.zeros((len(image_paths), 5), dtype=int)
    labels = translate_labels(load_labels(image_paths))
    for i, lbl in enumerate(labels):
        lbl_ds[i, :] = lbl
    return lbl_ds


def zipped_dataset(image_paths):
    img_ds = images_dataset(image_paths)
    lbl_ds = labels_dataset(image_paths)
    return img_ds, lbl_ds


class BatchGen(object):
    def __init__(self, paths, batch_size, label):
        self.label = label
        self.paths = paths
        self.batch_size = batch_size
        self.idx = 0
        self.translator = outputs()
        self.rev_translator = reversed_outputs()
        self.out_num = len(self.translator)
        self.dsize = len(self.paths)

    def generate(self):
        while True:
            yield self.load_batch()

    def load_batch(self):
        if self.idx + self.batch_size > self.dsize:
            random.shuffle(self.paths)
            self.idx = 0
        lb, ub = self.idx, self.idx+self.batch_size
        xdata, ydata = zipped_dataset(self.paths[lb: ub])
        ydata = utils.to_categorical(ydata, self.out_num)
        self.idx = self.idx + self.batch_size
        return xdata, ydata


def split_paths(paths, x, y, z):
    assert(x + y + z <= 1)
    z = int((x + y + z) * len(paths))
    y = int((x + y) * len(paths))
    x = int(x * len(paths))
    return paths[:x], paths[x:y], paths[y:z]


def split_paths_fixed(paths, x, y, z):
    assert(x + y + z <= len(paths))
    return paths[:x], paths[x:x+y], paths[x+y:x+y+z]


def create_generators(img_root, batch_size, slices=(0.5, 0.3, 0.2), fixed=False):
    paths = get_shuffled_image_paths(img_root)
    if fixed:
        train, valid, test = split_paths_fixed(paths, *slices)
    else:
        train, valid, test = split_paths(paths, *slices)
    train = BatchGen(train, batch_size, label='TrainGen')
    valid = BatchGen(valid, batch_size, label='ValidGen')
    test = BatchGen(test, batch_size, label='TestGen')
    return train, valid, test


def create_datasets(img_root, slices=(0.6, 0.3, 0.1)):
    paths = get_shuffled_image_paths(img_root)
    train, valid, test = split_paths(paths, *slices)
    train = zipped_dataset(train)
    valid = zipped_dataset(valid)
    test = zipped_dataset(test)
    return train, valid, test


def save_model(model, filename="model"):
    model_json = model.to_json()
    with open(f"{filename}.json", "w") as json_file:
        json_file.write(model_json)
    model.save_weights(f"{filename}.h5")


def decode(label):
    decoded = [label[i, :].index(max(label[i, :]) for i in range(5))]
    return translate_lbl(decoded, reversed_outputs())


def save_history(history, filename="history.png"):
    plt.figure(1)
    for i, metric in enumerate(['acc', 'loss']):
        plt.subplot(2, 1, i+1)
        plt.grid(True)
        plt.plot(hist.history[metric])
        plt.plot(hist.history[f'val_{metric}'])
        plt.title(f'model {metric}')
        plt.ylabel(metric)
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(f"{filename}")


np.random.seed(seed=4)

batch = 20
epochs = 40
dropout = 0.2
out_nums = 36
timesteps = 5
img_channels = 4
img_x, img_y = 50, 200
img_root = 'images'

os.mkdir(f"models/d{dropout}b{batch_size}")
model_path = f"models/drop{dropout}batch{batch_size}/" + \
    "e{epoch:02d}_va{val_acc:.2f}_vl{val_loss:.2f}.hdf5"
plot_path = f"batch20_drop20.png"

ishape = (batch_size, timesteps, img_x, img_y, img_channels)
conv_ishape = (batch_size, img_x, img_y, img_channels)
wrap_shape = (timesteps, img_x, img_y, img_channels)

train, valid, test = create_generators(
    img_root,
    batch_size
)

model = tf.keras.models.Sequential([
    layers.TimeDistributed(
        layers.Conv2D(
            1, (2, 2),
            activation='relu',
            input_shape=conv_ishape,
            padding='same'),
        input_shape=(wrap_shape)
    ),  # 5, 5, 50, 200, 1
    layers.TimeDistributed(
        layers.MaxPool2D(
            pool_size=(2, 2),
            strides=(2, 2),
            padding='same')
    ),  # 5, 5, 25, 100, 1
    layers.TimeDistributed(
        layers.Conv2D(
            1, (2, 2),
            activation='relu',
            input_shape=conv_ishape,
            padding='same'),
        input_shape=(wrap_shape)
    ),  # 5, 5, 25, 100, 1
    layers.TimeDistributed(
        layers.MaxPool2D(
            pool_size=(2, 2),
            strides=(2, 2),
            padding='same')
    ),  # 5, 5, 13, 50, 1
    layers.TimeDistributed(
        layers.Conv2D(
            1, (2, 2),
            activation='relu',
            input_shape=conv_ishape,
            padding='same'),
        input_shape=(wrap_shape)
    ),  # 5, 5, 13, 50, 1
    layers.TimeDistributed(
        layers.MaxPool2D(
            pool_size=(2, 2),
            strides=(2, 2),
            padding='same')
    ),  # 5, 5, 7, 25, 1
    layers.Reshape((timesteps, 7*25)),
    layers.LSTM(
        7*25,
        return_sequences=True,
        dropout=dropout
    ),
    layers.TimeDistributed(
        layers.Dense(out_nums)
    ),
    layers.Activation('softmax')
])

model.compile(
    loss='categorical_crossentropy',
    optimizer='adam',
    metrics=['acc']
)

cp = callbacks.ModelCheckpoint(
    filepath=model_path,
    monitor='val_acc',
    save_best_only=True,
    mode='max',
    verbose=1
)

es = callbacks.EarlyStopping(
    monitor='val_loss',
    min_delta=0.01,
    patience=2,
    verbose=1
)

hist = model.fit_generator(
    train.generate(),
    train.dsize//batch_size,
    epochs,
    validation_data=valid.generate(),
    validation_steps=valid.dsize//batch_size,
    verbose=1,
    callbacks=[cp, es]
)

score, acc = model.evaluate_generator(
    test.generate(),
    steps=test.dsize//batch_size,
    verbose=1
)

plt.clf()
save_history(hist, filename=plot_path)
